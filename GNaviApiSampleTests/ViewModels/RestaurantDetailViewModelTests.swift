//
//  RestaurantDetailViewModelTests.swift
//  GNaviApiSampleTests
//
//  Created by 星野友紀央 on 2019/07/20.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import XCTest
import RxSwift
@testable import GNaviApiSample

/// RestaurantDetailViewModel のテストクラス
class RestaurantDetailViewModelTests: XCTestCase {
    
    /// Stub
    class CallableTelephony: TelephonyProtocol {
        var isCallable: Bool {
            return true
        }
    }

    /// Stub
    class NotCallableTelephony: TelephonyProtocol {
        var isCallable: Bool {
            return false
        }
    }
    
    override func setUp() {
    }

    override func tearDown() {
    }
    
    // MARK: - Test for Reload

    /// reloadCellDataList を行なった際に cellDataList にデータが流れてくることをテストする
    func testReloadCellDataList() {
        
        let expectation = XCTestExpectation(description: "Received a new cellDataList")
        
        let viewModel = RestaurantDetailViewModel(telephony: CallableTelephony())
        
        let bag = DisposeBag()
        
        viewModel.cellDataList
            .skip(1)    // binding時に流れてくるデータは無視
            .subscribe(onNext: { _ in
                expectation.fulfill()
            }).disposed(by: bag)
        
        viewModel.reloadCellDataList(with: Restaurant())
        
        wait(for: [expectation], timeout: 1)
    }

    // MARK: - Test for Image Cell Data
    
    /// セルデータの生成が正しく行われることをテスト for 画像セル
    func testCreateCellDataListForImageCell() {
        
        let viewModel = RestaurantDetailViewModel(telephony: CallableTelephony())

        // 正しいURLがセットされたレストラン情報を元にセルデータ配列をリロードした際に、.imageセルデータが含まれること
        do {
            let urlStr = "http://www.yahoo.co.jp"
            var restaurant = Restaurant()
            restaurant.imageUrlInformation = Restaurant.ImageUrlInformation(urlStr: urlStr)
            
            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .image(let url) = $0 {
                    return url.absoluteString == urlStr
                }
                return false
            }
            XCTAssert(contains, "[Error] Create Image Cell Data")
        }
        
        // 間違ったURLがセットされたレストラン情報を元にセルデータ配列をリロードした際に、.imageセルデータが含まれないこと
        do {
            var restaurant = Restaurant()
            restaurant.imageUrlInformation = Restaurant.ImageUrlInformation(urlStr: "aaaaa")

            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .image = $0 {
                    return true
                }
                return false
            }
            XCTAssert(!contains, "[Error] Create Image Cell Data")
        }

        // 空文字列のURLがセットされたレストラン情報を元にセルデータ配列をリロードした際に、.imageセルデータが含まれないこと
        do {
            var restaurant = Restaurant()
            restaurant.imageUrlInformation = Restaurant.ImageUrlInformation(urlStr: "")

            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .image = $0 {
                    return true
                }
                return false
            }
            XCTAssert(!contains, "[Error] Create Image Cell Data")
        }
        
        // URLがセットレストラン情報を元にセルデータ配列をリロードした際に、.imageセルデータが含まれないこと
        do {
            let restaurant = Restaurant()

            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .image = $0 {
                    return true
                }
                return false
            }
            XCTAssert(!contains, "[Error] Create Image Cell Data")
        }
    }
    
    // MARK: -

    /// セルデータの生成が正しく行われることをテスト for タイトルセル
    func testCreateCellDataListForTitleCell() {
        
        let viewModel = RestaurantDetailViewModel(telephony: CallableTelephony())
        
        // 名称/カテゴリ/住所がセットされたレストラン情報を元にセルデータ配列をリロードした際に、.titleセルデータが含まれること
        do {
            let name     = "レストラン"
            let category = "カテゴリ"
            let address  = "アドレス"
            var restaurant = Restaurant()
            restaurant.name     = name
            restaurant.category = category
            restaurant.address  = address

            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .title(let n, let c, let a) = $0 {
                    return n == name && c == category && a == address
                }
                return false
            }
            XCTAssert(contains, "[Error] Create Title Cell Data")
        }

        // 名称だけがセットされたレストラン情報を元にセルデータ配列をリロードした際に、.titleセルデータが含まれること
        do {
            let name = "レストラン"
            var restaurant = Restaurant()
            restaurant.name = name
            
            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .title(let n, _, _) = $0 {
                    return n == name
                }
                return false
            }
            XCTAssert(contains, "[Error] Create Title Cell Data")
        }

        // 空の名称がセットされたレストラン情報を元にセルデータ配列をリロードした際に、.titleセルデータが含まれないこと
        do {
            var restaurant = Restaurant()
            restaurant.name = ""

            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .title(_, _, _) = $0 {
                    return true
                }
                return false
            }
            XCTAssert(!contains, "[Error] Create Title Cell Data")
        }

        // 名称がnilのレストラン情報を元にセルデータ配列をリロードした際に、.titleセルデータが含まれないこと
        do {
            let restaurant = Restaurant()
            
            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .title(_, _, _) = $0 {
                    return true
                }
                return false
            }
            XCTAssert(!contains, "[Error] Create Title Cell Data")
        }
    }
    
    // MARK: -

    /// セルデータの生成が正しく行われることをテスト for ホームページセル
    func testCreateCellDataListForHomePageCell() {
        
        let viewModel = RestaurantDetailViewModel(telephony: CallableTelephony())
        
        // 正しいURLがセットされたレストラン情報を元にセルデータ配列をリロードした際に、ホームページセルデータが含まれること
        do {
            let urlStr = "http://www.yahoo.co.jp"
            var restaurant = Restaurant()
            restaurant.urlStr = urlStr
            
            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .general(let name, let value, let behavior) = $0 {
                    if name == R.string.localizable.hp() && value == urlStr {
                        if case .openSafari(let url) = behavior, url.absoluteString == urlStr {
                            return true
                        }
                    }
                }
                return false
            }
            XCTAssert(contains, "[Error] Create HomePage Cell Data")
        }

        // 正しくないURLがセットされたレストラン情報を元にセルデータ配列をリロードした際に、ホームページセルデータが含まれないこと
        do {
            let urlStr = "aaaa"
            var restaurant = Restaurant()
            restaurant.urlStr = urlStr
            
            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .general(let name, _, _) = $0 {
                    if name == R.string.localizable.hp() {
                        return true
                    }
                }
                return false
            }
            XCTAssert(!contains, "[Error] Create HomePage Cell Data")
        }
    }

    // MARK: -

    /// セルデータの生成が正しく行われることをテスト for 電話セル
    func testCreateCellDataListForTelCell() {
        
        // [電話がかけられる端末]電話番号を含むレストラン情報を元にセルデータ配列をリロードした際に、Call可能な電話番号セルが含まれること
        do {
            let viewModel = RestaurantDetailViewModel(telephony: CallableTelephony())
            
            let tel = "090-1234-5678"
            var restaurant = Restaurant()
            restaurant.tel = tel
            
            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .general(let name, let value, let behavior) = $0 {
                    if name == R.string.localizable.tel() && value == tel {
                        if case .call(let url) = behavior, url.absoluteString == tel.telUrl()?.absoluteString {
                            return true
                        }
                    }
                }
                return false
            }
            XCTAssert(contains, "[Error] Create Tel Cell Data")
        }
        
        // [電話がかけられない端末]電話番号を含むレストラン情報を元にセルデータ配列をリロードした際に、Call不可能な電話番号セルが含まれること
        do {
            let viewModel = RestaurantDetailViewModel(telephony: NotCallableTelephony())
            
            let tel = "090-1234-5678"
            var restaurant = Restaurant()
            restaurant.tel = tel
            
            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .general(let name, let value, let behavior) = $0 {
                    if name == R.string.localizable.tel() && value == tel {
                        if case .none = behavior {
                            return true
                        }
                    }
                }
                return false
            }
            XCTAssert(contains, "[Error] Create Tel Cell Data")
        }
        
        // 正しい電話番号を含まないレストラン情報を元にセルデータ配列をリロードした際に、電話番号セルが含まれない
        do {
            let viewModel = RestaurantDetailViewModel(telephony: CallableTelephony())
            
            let tel = "090"
            var restaurant = Restaurant()
            restaurant.tel = tel
            
            let list = viewModel.createCellDataList(with: restaurant)
            let contains = list.contains {
                if case .general(let name, _, _) = $0 {
                    if name == R.string.localizable.tel() {
                        return true
                    }
                }
                return false
            }
            XCTAssert(!contains, "[Error] Create Tel Cell Data")
        }
    }
    
    // MARK: -
    
    /// セルデータの生成が正しく行われることをテスト for その他のセル
    func testCreateCellDataListForFaxCell() {

        let viewModel = RestaurantDetailViewModel(telephony: CallableTelephony())

        // チェックターゲット
        enum Target {
            case fax
            case openTime
            case holiday
            case pr
            
            var name: String {
                switch self {
                case .fax:      return R.string.localizable.fax()
                case .openTime: return R.string.localizable.openTime()
                case .holiday:  return R.string.localizable.holiday()
                case .pr:       return R.string.localizable.pr()
                }
            }
        }
        
        let targets: [Target] = [.fax, .openTime, .holiday, .pr]
        
        for target in targets {
            
            // 正しい情報を含むレストラン情報を元にセルデータ配列をリロードした際に、対象のセルが含まれること
            do {
                let targetValue = "aaaa"
                var restaurant = Restaurant()
                switch target {
                case .fax:      restaurant.fax      = targetValue
                case .openTime: restaurant.openTime = targetValue
                case .holiday:  restaurant.holiday  = targetValue
                case .pr:       restaurant.prInformation = Restaurant.PRInformation(short: nil, long: targetValue)
                }
                
                let list = viewModel.createCellDataList(with: restaurant)
                let contains = list.contains {
                    if case .general(let name, let value, let behavior) = $0 {
                        if name == target.name, value == targetValue, case .none = behavior {
                            return true
                        }
                    }
                    return false
                }
                XCTAssert(contains, "[Error] Create \(target.name) Cell Data")
            }
        
            // 正しい情報を含まないレストラン情報を元にセルデータ配列をリロードした際に、対象のセルが含まれないこと
            do {
                let targetValue = ""
                var restaurant = Restaurant()
                switch target {
                case .fax:      restaurant.fax      = targetValue
                case .openTime: restaurant.openTime = targetValue
                case .holiday:  restaurant.holiday  = targetValue
                case .pr:       restaurant.prInformation = Restaurant.PRInformation(short: nil, long: targetValue)
                }

                let list = viewModel.createCellDataList(with: restaurant)
                let contains = list.contains {
                    if case .general(let name, let value, let behavior) = $0 {
                        if name == target.name, value == targetValue, case .none = behavior {
                            return true
                        }
                    }
                    return false
                }
                XCTAssert(!contains, "[Error] Create \(target.name) Cell Data")
            }
        }
    }
}
