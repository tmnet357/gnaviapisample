## 概要
私が普段どのような設計/コーディングを行っているかを簡単に説明するための(とても簡単な)サンプルプログラムです。


## 開発環境
OS: macOS Mojave (10.14.5)  
Xcode: 10.3(10G8)  
Swift: 5.0  
Carthage: 0.33.0  
Cocoa Pods: 1.7.5  
Target OS: iOS 11.0〜


## ビルドと実行手順
carthage と cocoapods による外部ライブラリの利用をしているため、リポジトリをクローンした後は下記コマンドを実行してください(carthage と CocoaPods は適切なバージョンをすでにインストール済みとします)。   
```$ carthage bootstrap --platform iOS
```  
```$ pod install
```  
その後、生成された GNaviApiSample.xcworkspace を開いてビルド＆実行してください。

 
## アプリ概要
・[ぐるなびのレストラン検索API](https://api.gnavi.co.jp/api/manual/restsearch/)を利用したレストラン検索のiOSアプリです。  

***[地図画面]***  
・アプリを起動すると**地図**と**青いピン**と**検索窓**が表示されます  
・検索窓にキーワードを入力して検索ボタンを押すと、青いピンを中心とした周辺のレストラン検索を行い、ヒットしたレストランの位置を**赤ピン**で表示します  
・0件ヒットの場合は、キーワードを変えるか、**地図の長押し**で青いピンの位置を変更してください  
・赤ピンをタップするとバルーンが表示されますので、バルーン内のアイコンをタップするとレストランの詳細画面に遷移します

***[レストラン詳細画面]***  
・APIで取得した情報をテーブルビューで表示します  
・HPのセルをタップするとSafariを開き、電話番号のセルをタップすると電話をかけます

## 利用ライブラリ
・Alamofire  
・AlamofireImage  
・LicensePlist  
・R.swift  
・RxSwift/RxCocoa  
・SVProgressHUD  
・SwiftLint

## 設計のポイント
・HTTP通信のモジュールとして HttpClient クラスを用意し(Alamofireのラッパークラス)、プロジェクトからは直接Alamofireを利用するのではなく、このクラスを利用して通信を行うようにしています(と言っても、現状で叩いているAPIは一つだけですが。。。)。このようにすることで、Alamofireのバージョンアップ時や、あるいは別の通信SDKに切り替えるときに、このクラスだけを修正すれば済むようになり、運用のコストダウンになります。  
  
・レストラン詳細画面ではUITableViewを使って各種情報を表示していますが、RestaurantDetailViewController がレストランの情報を受け取ったときに、ViewModel側で表示すべきセルのデータ配列を生成し、ViewControllerではデータ種別に応じたCellを、Cellにはデータが保持する表示内容をセットするだけで済み、ViewController, Cell からビジネスロジックを極力排除した作りになっています(もともと大したビジネスロジックは無いため、この設計による恩恵は現状ほとんどありませんが、取得したデータから複雑なデータを生成したい場合は、ほぼModel(Restaurant or RestaurantDetailViewModel)側だけの修正で済みます。(このあたりはユニットテストで確認したほうがわかりやすいため、時間を見つけてテストコードを書きます)
  

## git-flow
今回は一人で行う簡単なアプリのため、develop ブランチ上で開発を行いましたが、チームで開発を行う時は、通常の git-flow に則り、master, release, develop, feature, hotfix にブランチを分けて機能開発・リリース・不具合修正などを行っております。
