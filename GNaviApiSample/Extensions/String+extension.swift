//
//  String+extension.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/23.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import Foundation

extension String {

    // 電話番号形式の文字列から電話番号のURLを生成する
    func telUrl() -> URL? {
        
        // 電話番号の形式でなければNG
        guard isTelNumberFormat() else {
            return nil
        }
        
        // ハイフンを取り除く
        let converted = self.replacingOccurrences(of: "-", with: "")
        
        // 頭
        return URL(string: "tel://\(converted)")
    }
    
    // 電話番号形式かどうかの判定
    func isTelNumberFormat() -> Bool {
        
        guard let regex = try? NSRegularExpression(pattern: "^\\d{2,4}-\\d{1,4}-\\d{4}$", options: []) else {
            return false
        }
        
        let matches = regex.matches(in: self, options: [], range: NSRange(location: 0, length: count))
        
        return !matches.isEmpty
    }
    
    // http もしくは https で始まる文字列か判定
    func hasHttpPrefix() -> Bool {
        return hasPrefix("http://") || hasPrefix("https://")
    }
}
