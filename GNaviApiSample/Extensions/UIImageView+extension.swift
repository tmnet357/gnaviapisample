//
//  UIImageView+extension.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/22.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import AlamofireImage
import UIKit

extension UIImageView {
    
    func fetchImage(with url: URL) {
        
        af_setImage(withURL: url) { [weak self] response in
            switch response.result {
            case .success(let image):
                self?.image = image
                
            case .failure:
                break
            }
            
        }
    }
}
