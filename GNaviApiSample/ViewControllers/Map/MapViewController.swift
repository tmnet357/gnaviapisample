//
//  MapViewController.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/20.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import MapKit
import RxCocoa
import RxSwift
import SVProgressHUD
import UIKit

/// 地図画面のViewController
class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textLengthLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    /// 検索の中心点を表すアノテーション
    private var searchingCenterAnnotation: MKPointAnnotation?
    
    /// レストランを表すアノテーション
    private var restaurantAnnotations = [RestaurantAnnotation]()

    private let viewModel = MapViewModel()

    private let bag = DisposeBag()
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    
        textField.placeholder = R.string.localizable.freeWordPlaceHolder()
        
        searchButton.setTitle(R.string.localizable.search(), for: .normal)
        cancelButton.setTitle(R.string.localizable.cancel(), for: .normal)

        // Bindingのセットアップ
        setupBinding()
     
        // 最初は皇居を表示する
        setMapRegion(to: viewModel.coordinateOfImperialPalace, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // 地図画面ではナビゲーションバーを表示しない
        navigationController?.isNavigationBarHidden = true
    }

    /// セグエによる画面遷移準備
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // レストラン詳細画面にレストラン情報をセットする
        if let vc = segue.destination as? RestaurantDetailViewController, let annotation = sender as? RestaurantAnnotation {
            vc.setRestaurant(annotation.restaurant)
        }
    }
    
    // MARK: - IBAction
    
    /// 検索ボタン押下時の処理
    @IBAction func searchButtonDidTap(_ sender: Any) {
        
        guard let searchingWord = textField.text else {
            return
        }

        // 検索リクエスト
        viewModel.request(searchingWord: searchingWord, coordinate: viewModel.searchingCenterCoordinate.value) { [weak self] result in
            
            // リクエスト失敗、もしくは成功しても0件だった場合にダイアログを表示する
            switch result {
            case .success(let hitCount):
                if hitCount == 0 {
                    self?.showErrorDialog(message: R.string.localizable.notFound())
                }
                
            case .failure:
                self?.showErrorDialog(message: R.string.localizable.connectionFailed())
            }
        }
    }
    
    /// キャンセルボタン押下時の処理
    @IBAction func cancelButtondidTap(_ sender: Any) {
        viewModel.cancelRequest()
    }
    
    /// 地図長押し時の処理
    @IBAction func mapViewDidLongPress(_ sender: UILongPressGestureRecognizer) {
        
        // タップ地点の座標
        let point = sender.location(in: mapView)
        let coordinate = mapView.convert(point, toCoordinateFrom: mapView)
        
        // 検索の中心点の座標を更新する
        viewModel.searchingCenterCoordinate.accept(coordinate)
    }
    
    // MARK: - Private
    
    /// Bindingのセットアップ
    private func setupBinding() {
        
        // 入力文字列長のObservable
        let textLength = textField.rx.text
            .map { return $0?.count ?? 0 }
            .share(replay: 1, scope: .forever)
        
        // 現在の文字数を表示
        textLength
            .map { R.string.localizable.searchTextLength($0) }
            .bind(to: textLengthLabel.rx.text)
            .disposed(by: bag)
        
        // 検索ワードが0文字以外かつリクエスト中でないときに検索ボタンを有効にする
        Observable.combineLatest(textLength, viewModel.isRequesting) { textLength, isRequesting in
            textLength != 0 && !isRequesting
        }
        .bind(to: searchButton.rx.isEnabled)
        .disposed(by: bag)
        
        // 検索の中心点が変わったときにアノテーションを貼り直す
        viewModel.searchingCenterCoordinate
            .subscribe(onNext: { [weak self] coordinate in
                
                //　既存のアノテーションを削除
                if let annotation = self?.searchingCenterAnnotation {
                    self?.mapView.removeAnnotation(annotation)
                    self?.searchingCenterAnnotation = nil
                }
                
                // 新しいアノテーションを貼る
                let annotation = MKPointAnnotation()
                annotation.coordinate = coordinate
                self?.mapView.addAnnotation(annotation)
                self?.searchingCenterAnnotation = annotation
            })
            .disposed(by: bag)
        
        // リクエスト中はインジケータを表示する
        viewModel.isRequesting
            .skip(1) // バインディング開始時に流れてくるデータは無視
            .subscribe(onNext: { isRequesting in
                if isRequesting {
                    SVProgressHUD.show()
                } else {
                    SVProgressHUD.dismiss()
                }
            }).disposed(by: bag)
        
        // リクエスト中はキャンセルボタンを有効にする
        viewModel.isRequesting
            .bind(to: cancelButton.rx.isEnabled )
            .disposed(by: bag)
        
        // 検索結果のレストランリストに更新がかかったら、店のアノテーションを貼り直す
        viewModel.restarurants
            .subscribe(onNext: { [weak self] restaurants in
                
                // レストランのアノテーションを更新
                self?.updateRestaurantAnnotations(with: restaurants)

            }).disposed(by: bag)
    }
    
    /// レストランのアノテーションを更新
    private func updateRestaurantAnnotations(with restaurants: [Restaurant]) {
        
        // 古いアノテーションを削除
        mapView.removeAnnotations(restaurantAnnotations)
        restaurantAnnotations.removeAll()
        
        // レストランリストからアノテーションリスト(nil含む)を生成
        let optionalAnnotations: [RestaurantAnnotation?] = restaurants.map {
            guard let coordinate = $0.coordinate, let name = $0.name, let address = $0.address else {
                return nil
            }
            
            let annotation = RestaurantAnnotation(restaurant: $0)
            annotation.coordinate = coordinate
            annotation.title = name
            annotation.subtitle = address
            return annotation
        }
        
        // アノテーションリストから nil のものを取り除く
        let annotations = optionalAnnotations.compactMap { $0 }
        
        // 新しいレストランアノテーションを地図に追加する
        mapView.addAnnotations(annotations)
        
        // アノテーションを保持する
        restaurantAnnotations.append(contentsOf: annotations)
    }
    
    // 地図のリージョン設定
    private func setMapRegion(to coordinate: CLLocationCoordinate2D, span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03), animated: Bool = true) {
        
        let region = MKCoordinateRegion(center: coordinate, span: span)

        mapView.setRegion(region, animated: animated)
    }
    
    // 検索用UIの表示/非表示
    private func setSearchingInterfaceHidden(_ isHidden: Bool) {
        searchContainerView.isHidden = isHidden
    }
    
    /// エラーダイアログの表示
    private func showErrorDialog(message: String) {
        
        let vc = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        vc.addAction(UIAlertAction(title: R.string.localizable.ok(), style: .default, handler: nil))
        
        present(vc, animated: true, completion: nil)
    }
}

// MARK: - MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    
    /// 地図の表示領域の変更開始時の処理
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        // 検索用UIを非表示
        setSearchingInterfaceHidden(true)
    }
    
    /// 地図の表示領域の変更終了時の処理
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        // 検索用UIを表示する
        setSearchingInterfaceHidden(false)
    }

    /// アノテーションに対応するViewインスタンスを返す
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let annotation = annotation as? RestaurantAnnotation {
            // レストランの場合
            return restaurantAnnotaionView(mapView, annotation: annotation)
        } else if let annotation = annotation as? MKPointAnnotation {
            // 検索の中心点の場合
            return searchingCenterAnnotaionView(mapView, annotation: annotation)
        }
        
        return nil
    }
    
    /// 検索の中心点のアノテーションのビュー
    private func searchingCenterAnnotaionView(_ mapView: MKMapView, annotation: MKPointAnnotation) -> MKAnnotationView? {
        
        let identifier = "centerPin"
        
        var pin = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        
        if pin == nil {
            pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            pin?.pinTintColor = .blue
            pin?.animatesDrop = false
        }
        
        pin?.annotation = annotation

        return pin
    }
    
    /// お店のアノテーションのビュー
    private func restaurantAnnotaionView(_ mapView: MKMapView, annotation: RestaurantAnnotation) -> MKAnnotationView? {
        
        let identifier = "restaurantPin"
        
        var pin = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        
        if pin == nil {
            pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            pin?.animatesDrop = true
            pin?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            pin?.canShowCallout = true
        }

        pin?.annotation = annotation

        return pin
    }
    
    /// アノテーションのコールアウトタップ時の処理
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        guard let annotation = view.annotation as? RestaurantAnnotation else {
            return
        }

        // レストラン詳細画面へ遷移
        performSegue(withIdentifier: "restaurant_detail", sender: annotation)
    }
}
