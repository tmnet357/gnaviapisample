//
//  RestaurantDetailViewController.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/22.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import RxSwift
import UIKit

/// レストラン詳細画面のViewController
class RestaurantDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView?
    
    private let viewModel = RestaurantDetailViewModel(telephony: Telephony.shared)
    
    private let bag = DisposeBag()
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Cellのnibを登録
        tableView?.register(RestaurantImageCell.nib,   forCellReuseIdentifier: RestaurantImageCell.reuseIdentifier)
        tableView?.register(RestaurantTitleCell.nib,   forCellReuseIdentifier: RestaurantTitleCell.reuseIdentifier)
        tableView?.register(RestaurantGeneralCell.nib, forCellReuseIdentifier: RestaurantGeneralCell.reuseIdentifier)

        tableView?.tableFooterView = UIView()
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 120

        // セルデータ配列に更新がかかったらTableViewをリロード
        viewModel.cellDataList
            .subscribe(onNext: { [weak self] _ in
                self?.tableView?.reloadData()
            })
            .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // 地図画面に戻れるようにナビゲーションバーを表示する
        navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Public
    
    /// レストラン情報をセット
    func setRestaurant(_ restaurant: Restaurant) {
        // セルデータ配列をリロード
        viewModel.reloadCellDataList(with: restaurant)
    }
}

// MARK: - UITableViewDataSource

extension RestaurantDetailViewController: UITableViewDataSource {
    
    /// セル数
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cellDataList.value.count
    }
    
    /// セルインスタンスを返す
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row < viewModel.cellDataList.value.count else {
            // 想定外のフロー
            return UITableViewCell()
        }
        
        // セルデータ
        let data = viewModel.cellDataList.value[indexPath.row]
        
        // セルデータに応じて、対応するセルインスタンスを返す
        switch data {
        case .image(let url):
            return imageCell(tableView, rowAt: indexPath, url: url)
        case .title(let name, let category, let address):
            return titleCell(tableView, rowAt: indexPath, name: name, category: category, address: address)
        case .general(let name, let value, let behavior):
            return generalCell(tableView, rowAt: indexPath, behavior: behavior, name: name, value: value)
        }
    }
    
    /// 画像セルインスタンスを返す
    private func imageCell(_ tableView: UITableView, rowAt indexPath: IndexPath, url: URL) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantImageCell.reuseIdentifier, for: indexPath)
        
        if let cell = cell as? RestaurantImageCell {
            cell.setImageUrl(url)
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    /// タイトルセルインスタンスを返す
    private func titleCell(_ tableView: UITableView, rowAt indexPath: IndexPath, name: String, category: String?, address: String?) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantTitleCell.reuseIdentifier, for: indexPath)
        
        if let cell = cell as? RestaurantTitleCell {
            cell.set(name: name, category: category, address: address)
        }

        cell.selectionStyle = .none

        return cell
    }
    
    /// 汎用セルインスタンスを返す
    private func generalCell(_ tableView: UITableView, rowAt indexPath: IndexPath, behavior: RestaurantDetailViewModel.SelectionBehavior, name: String, value: String) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantGeneralCell.reuseIdentifier, for: indexPath)
        
        if let cell = cell as? RestaurantGeneralCell {
            cell.set(name: name, value: value, valueColor: behavior.valueTextColor)
        }
        
        cell.selectionStyle = behavior.selectionStyle

        return cell
    }
}

// MARK: - UITableViewDelegate

extension RestaurantDetailViewController: UITableViewDelegate {
    
    /// セルの高さ
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /// セルタップ時の処理
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        guard indexPath.row < viewModel.cellDataList.value.count else {
            return
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        // セルデータ
        let data = viewModel.cellDataList.value[indexPath.row]
        
        // タップの挙動に応じた処理
        switch data.selectionBehavior {
        case .none:
            break
        case .openSafari(let url):
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        case .call(let telUrl):
            UIApplication.shared.open(telUrl, options: [:], completionHandler: nil)
        }

    }
}
