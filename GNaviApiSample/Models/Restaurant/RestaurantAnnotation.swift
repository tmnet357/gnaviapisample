//
//  RestaurantAnnotation.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/21.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import Foundation
import MapKit

// レストラン専用アノテーション
class RestaurantAnnotation: MKPointAnnotation {
    
    var restaurant: Restaurant
    
    init(restaurant: Restaurant) {
        self.restaurant = restaurant
        super.init()
    }
}
