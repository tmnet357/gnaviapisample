//
//  Restaurant.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/24.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import Foundation
import MapKit

/// レストランデータ
struct Restaurant: Codable {
    
    /// 画像URL情報
    struct ImageUrlInformation: Codable {
        var urlStr : String?
        
        enum CodingKeys: String, CodingKey {
            case urlStr = "shop_image1"
        }
    }
    
    /// PR情報
    struct PRInformation: Codable {
        var short   : String?
        var long    : String?
        
        enum CodingKeys: String, CodingKey {
            case short = "pr_short"
            case long  = "pr_long"
        }
    }
    
    var id                  : String?
    var name                : String?
    var category            : String?
    var urlStr              : String?
    var address             : String?
    var latitudeStr         : String?
    var longitudeStr        : String?
    var tel                 : String?
    var fax                 : String?
    var openTime            : String?
    var holiday             : String?
    var imageUrlInformation : ImageUrlInformation?
    var prInformation       : PRInformation?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case category
        case urlStr                 = "url"
        case address
        case latitudeStr            = "latitude"
        case longitudeStr           = "longitude"
        case tel
        case fax
        case openTime               = "opentime"
        case holiday
        case imageUrlInformation    = "image_url"
        case prInformation          = "pr"
    }
    
    /// 座標
    var coordinate: CLLocationCoordinate2D? {
        guard let latStr = latitudeStr, let lonStr = longitudeStr, let lat = CLLocationDegrees(latStr), let lon = CLLocationDegrees(lonStr) else {
            return nil
        }
        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
    
    /// 画像URL
    var imageUrlStr: String? {
        return imageUrlInformation?.urlStr
    }
    
    /// PR文
    var pr: String? {
        return prInformation?.long
    }
    
}
