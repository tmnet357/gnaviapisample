//
//  Telephony.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/28.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import CoreTelephony
import UIKit

/// Telephonyプロトコル
protocol TelephonyProtocol {
    /// 電話がかけられるかどうか
    var isCallable: Bool { get }
}

/// CoreTelephony のラッパー
class Telephony: TelephonyProtocol {
    
    static let shared = Telephony()

    private let info = CTTelephonyNetworkInfo()

    private init() {}
    
    /// 電話がかけられるかどうか
    var isCallable: Bool {
        
        // 機内モード/SIMなし/キャリア通信県外のときはNG
        // https://developer.apple.com/documentation/coretelephony/ctcarrier/1620317-isocountrycode
        guard info.subscriberCellularProvider?.isoCountryCode != nil else {
            return false
        }
        
        // iPhone以外はNG(SIMの入ったiPadもNG)
        guard UIDevice.current.userInterfaceIdiom == .phone else {
            return false
        }

        return true
    }
}
