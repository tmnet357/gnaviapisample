//
//  HttpClient.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/20.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import Alamofire
import Foundation

/*
 * HTTP通信モジュール
 *  プロジェクトでは直接 Alamofire は使わず、このモジュールを介して通信を行うことで
 *  Alamofire のバージョンアップ、あるいは他の通信ライブラリへ移行する際は、このクラスの修正だけで済む
 */

/// HTTPメソッド定義(HTTPMethodのラッパー)
enum MyAppHttpMethod {
    
    case get
    case post
    
    /// Alamofire の HTTPMethod への変換(外部には見せたくない部分)
    fileprivate var httpMethod: HTTPMethod {
        switch self {
        case .get:  return .get
        case .post: return .post
        }
    }
}

/// リクエストパラメータ
typealias MyAppRequestParameter = [String: Any]

/// リクエストが準拠すべきプロトコル(ここは外部に見せる部分なので、Alamofireに依存しない作りを保つ)
protocol MyAppRequest {

    /// レスポンスタイプ
    associatedtype ResponseType: SampleAppResponse
  
    /// URL
    var urlStr: String { get }
    
    /// HTTPメソッド
    var method: MyAppHttpMethod { get }
    
    /// パラメータ
    var parameters: MyAppRequestParameter? { get }
    
    /// APIキー(デフォルト実装済み)
    var apiKey: String { get }
}

/// デフォルト実装
extension MyAppRequest {
    var apiKey: String {
        return "10f1b683a4a128fc1a940c1deaa92e92"
    }
    
    /// Alamofire用のパラメータ(外部には見せたくない部分)
    fileprivate func alamofireParameters() -> Parameters? {
        //　今は MyAppRequestParameter = Parameters なのでそのまま返す
        return parameters
    }
}

/// レスポンスが準拠すべきプロトコル
protocol SampleAppResponse: Codable {
}

/// エラー
enum MyAppError: Error {
    /// 不明なエラー
    case unknown
    /// キャンセルされた
    case canceled
    /// エラーレスポンスを受信した
    case errorResponse(response: ErrorResponse)
    /// デコードでエラーが発生した
    case decode
}

/// HTTPクライアントモジュール
class HttpClient {
    
    /// リクエストキャンセルを受け付けるために、リクエストを保持する
    private var _request: Request?
    
    /**
     リクエスト
     - parameter request: リクエストインスタンス
     - parameter didSucceed: 成功時ハンドラ
     - parameter didFaile: 失敗時ハンドラ
     - returns: リクエスト自体の成否(リクエスト中に再度リクエストを行うとfalseを返す)
     */
    @discardableResult
    func request<R: MyAppRequest>(_ request: R, didSucceed: @escaping (R.ResponseType) -> Void, didFail: @escaping (MyAppError) -> Void) -> Bool {
        
        guard _request == nil else {
            return false
        }
        
        _request = Alamofire.request(request.urlStr, method: request.method.httpMethod, parameters: request.alamofireParameters()).responseData { [weak self] response in
         
            defer {
                self?._request = nil
            }
            
            guard let rawData = response.result.value else {
                didFail(.unknown)
                return
            }

            if let str = String(data: rawData, encoding: .utf8) {
                print(str)
            }

            // 先にエラーレスポンスかどうかチェック
            if let response = try? JSONDecoder().decode(ErrorResponse.self, from: rawData) {
                didFail(.errorResponse(response: response))
                return
            }
            
            // リクエストに応じたレスポンス型にデコード
            guard let data = try? JSONDecoder().decode(R.ResponseType.self, from: rawData) else {
                didFail(.decode)
                return
            }

            didSucceed(data)
        }
        
        return true
    }

    /**
     キャンセル
     */
    func cancel() {
        _request?.cancel()
        _request = nil
    }
}
