//
//  ErrorResponse.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/24.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import Foundation

// エラーレスポンス
struct ErrorResponse: Codable {
    
    struct Error: Codable {
        let code: Int?
        let message: String?
    }
    
    var errors: [Error]
    
    enum CodingKeys: String, CodingKey {
        case errors = "error"
    }
}
