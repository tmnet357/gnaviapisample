//
//  RestaurantSearchRequest.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/21.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import UIKit
import MapKit

/// レストラン検索リクエスト　API仕様ページ https://api.gnavi.co.jp/api/manual/restsearch/
struct RestaurantSearchRequest: MyAppRequest {
    
    /// 検索半径
    enum Radius: Int {
        /// 300m
        case m300   = 1
        /// 500m
        case m500
        /// 1000m
        case m1000
        /// 3000m
        case m3000
    }

    typealias ResponseType = RestaurantSearchResponse

    var urlStr: String {
        return "https://api.gnavi.co.jp/RestSearchAPI/v3/"
    }
    
    var method: MyAppHttpMethod {
        return .get
    }
    
    var parameters: MyAppRequestParameter? {
        return [
            "keyid"         : apiKey,
            "freeword"      : searchingWord,
            "latitude"      : coordinate.latitude,
            "longitude"     : coordinate.longitude,
            "range"         : radius.rawValue
        ]
    }
    
    private let searchingWord   : String
    private let coordinate      : CLLocationCoordinate2D
    private let radius          : Radius

    init(searchingWord: String, coordinate: CLLocationCoordinate2D, radius: Radius) {
        self.searchingWord  = searchingWord
        self.coordinate     = coordinate
        self.radius         = radius
    }
}

/// レストラン検索レスポンス
struct RestaurantSearchResponse: SampleAppResponse {
    
    var totalHitCount   : Int?
    var hitPerPage      : Int?
    var pageOffset      : Int?
    var restaurnts      : [Restaurant]
    
    enum CodingKeys: String, CodingKey {
        case totalHitCount  = "total_hit_count"
        case hitPerPage     = "hit_per_page"
        case pageOffset     = "page_offset"
        case restaurnts     = "rest"
    }
}
