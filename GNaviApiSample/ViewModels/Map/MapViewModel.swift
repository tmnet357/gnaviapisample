//
//  MapViewModel.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/21.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import Foundation
import MapKit
import RxSwift
import RxCocoa

/// 地図画面のViewModel
class MapViewModel {
    
    /// 検索結果
    enum SearchResult {
        case success(hitCount: Int)
        case failure
    }
    
    /// 皇居の座標
    let coordinateOfImperialPalace = CLLocationCoordinate2DMake(35.685_175, 139.752_8)
    
    /// 検索の中心点の座標
    let searchingCenterCoordinate: BehaviorRelay<CLLocationCoordinate2D>
    
    /// リクエスト中か否か Observable
    var isRequesting: Observable<Bool> {
        return isRequestingSubject.asObserver()
    }
    private let isRequestingSubject = BehaviorSubject<Bool>(value: false)

    /// レストランリスト Obaservable
    var restarurants: Observable<[Restaurant]> {
        return restaurantsSubject.asObserver()
    }
    private let restaurantsSubject = PublishSubject<[Restaurant]>()

    /// HTTPクライアント
    private let httpClient = HttpClient()
    
    init() {
        // 検索の中心点の初期値を皇居にセットする
        searchingCenterCoordinate = BehaviorRelay<CLLocationCoordinate2D>(value: coordinateOfImperialPalace)
    }
    
    /**
     検索リクエスト
     - parameter searchingWord: 検索ワード
     - parameter coordinate: 検索の中点座標
     - parameter completion: 完了ハンドラ
     */
    func request(searchingWord: String, coordinate: CLLocationCoordinate2D, completion: @escaping (SearchResult) -> Void) {
        
        let request = RestaurantSearchRequest(searchingWord: searchingWord, coordinate: coordinate, radius: .m3000)
        
        // リクエスト中
        isRequestingSubject.onNext(true)
        
        httpClient.request(
            request,
            didSucceed: { [weak self] data in
                
                completion(.success(hitCount: data.restaurnts.count))

                // リクエスト完了
                self?.isRequestingSubject.onNext(false)

                // レストランリストを更新
                self?.restaurantsSubject.onNext(data.restaurnts)
                
            }, didFail: { [weak self] error in

                // 0件ヒット
                switch error {
                case .unknown, .decode, .canceled:
                    completion(.failure)
                case .errorResponse:
                    completion(.success(hitCount: 0))
                }

                // リクエスト完了
                self?.isRequestingSubject.onNext(false)
                
                // レストランリストを空にする
                self?.restaurantsSubject.onNext([])
            })
    }
    
    /// リクエストのキャンセル
    func cancelRequest() {
        httpClient.cancel()
    }
}
