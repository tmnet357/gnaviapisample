//
//  RestaurantDetailViewModel.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/22.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

/// レストラン詳細画面のViewModel
class RestaurantDetailViewModel {
    
    // MARK: - Inner Type
    
    /// セル選択時の挙動
    enum SelectionBehavior {
        /// 何もしない
        case none
        /// サファリで開く
        case openSafari(url: URL)
        /// 電話を掛ける
        case call(tel: URL)
        
        /// セルのセレクションスタイル
        var selectionStyle: UITableViewCell.SelectionStyle {
            switch self {
            case .none:         return .none
            case .openSafari:   return .default
            case .call:         return .default
            }
        }
        
        /// 値のテキストカラー
        var valueTextColor: UIColor {
            switch self {
            case .none:         return .black
            case .openSafari:   return .blue
            case .call:         return .blue
            }
        }
    }
    
    /// セルデータ
    enum CellData {
        /// 画像セル
        case image(url: URL)
        /// タイトルセル
        case title(name: String, category: String?, address: String?)
        /// その他の汎用セル
        case general(name: String, value: String, behavior: SelectionBehavior)
        
        /// セル選択時の挙動
        var selectionBehavior: SelectionBehavior {
            switch self {
            case .image:                        return .none
            case .title:                        return .none
            case .general(_, _, let behavior):  return behavior
            }
        }
    }
    
    // MARK: - Properites

    /// セルデータ配列 Observable
    let cellDataList = BehaviorRelay<[CellData]>(value: [])
    
    /// Telephonyインスタンス(DI)
    private let telephony: TelephonyProtocol
    
    // MARK: - Public Method
    
    /// 初期化
    init(telephony: TelephonyProtocol) {
        self.telephony = telephony
    }
    
    /// セルデータ配列のリロード
    func reloadCellDataList(with restaurant: Restaurant) {

        cellDataList.accept(createCellDataList(with: restaurant))
    }

    /// レストラン情報からセルデータ配列を生成
    func createCellDataList(with restaurant: Restaurant) -> [CellData] {
        
        var list = [CellData]()
        
        // 画像セルデータ
        if let str = restaurant.imageUrlStr, str.hasHttpPrefix(), let url = URL(string: str) {
            list.append(.image(url: url))
        }
        
        // 名称セルデータ(名称さえあれば、カテゴリとレストランはnilでも良い)
        if let name = restaurant.name, !name.isEmpty {
            list.append(.title(name: name, category: restaurant.category, address: restaurant.address))
        }
        
        // ホームページセルデータ
        if let str = restaurant.urlStr, str.hasHttpPrefix(), let url = URL(string: str) {
            list.append(.general(name: R.string.localizable.hp(), value: str, behavior: .openSafari(url: url)))
        }
        
        // 電話番号セルデータ
        if let tel = restaurant.tel, let telUrl = tel.telUrl() {
            if telephony.isCallable {
                list.append(.general(name: R.string.localizable.tel(), value: tel, behavior: .call(tel: telUrl)))
            } else {
                list.append(.general(name: R.string.localizable.tel(), value: tel, behavior: .none))
            }
        }
        
        // FAXセルデータ
        if let fax = restaurant.fax, !fax.isEmpty {
            list.append(.general(name: R.string.localizable.fax(), value: fax, behavior: .none))
        }
        
        // 営業時間セルデータ
        if let openTime = restaurant.openTime, !openTime.isEmpty {
            list.append(.general(name: R.string.localizable.openTime(), value: openTime, behavior: .none))
        }
        
        // 休業日セルデータ
        if let holiday = restaurant.holiday, !holiday.isEmpty {
            list.append(.general(name: R.string.localizable.holiday(), value: holiday, behavior: .none))
        }
        
        // PRセルデータ
        if let pr = restaurant.pr, !pr.isEmpty {
            list.append(.general(name: R.string.localizable.pr(), value: pr, behavior: .none))
        }

        return list
    }
}
