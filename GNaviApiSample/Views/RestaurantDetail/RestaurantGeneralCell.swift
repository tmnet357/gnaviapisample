//
//  RestaurantGeneralCell.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/22.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import UIKit

/// レストラン詳細汎用セル(項目名/項目値)
class RestaurantGeneralCell: UITableViewCell {
    
    static let nib = UINib(nibName: "RestaurantGeneralCell", bundle: nil)
    static let reuseIdentifier = "RestaurantGeneralCell"

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
   
    /// 項目名/値/値の文字色をセット
    func set(name: String, value: String, valueColor: UIColor) {
        nameLabel.text  = name
        valueLabel.text = value
        valueLabel.textColor = valueColor
    }
}
