//
//  RestaurantTitleCell.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/22.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import UIKit

/// レストラン詳細タイトル(名称/カテゴリ/住所)セル
class RestaurantTitleCell: UITableViewCell {
    
    static let nib = UINib(nibName: "RestaurantTitleCell", bundle: nil)
    static let reuseIdentifier = "RestaurantTitleCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    /// 名称/カテゴリ/住所をセット
    func set(name: String, category: String?, address: String?) {
        nameLabel.text     = name
        categoryLabel.text = category
        addressLabel.text  = address
    }
}
