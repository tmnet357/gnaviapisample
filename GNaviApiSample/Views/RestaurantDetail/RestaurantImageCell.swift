//
//  RestaurantImageCell.swift
//  GNaviApiSample
//
//  Created by 星野友紀央 on 2019/07/22.
//  Copyright © 2019 tmnet357. All rights reserved.
//

import UIKit

/// レストラン詳細画像セル
class RestaurantImageCell: UITableViewCell {

    static let nib = UINib(nibName: "RestaurantImageCell", bundle: nil)
    static let reuseIdentifier = "RestaurantImageCell"
    
    @IBOutlet weak var restaurantImageView: UIImageView!
    
    /// 画像URLをセット
    func setImageUrl(_ url: URL) {
        restaurantImageView.fetchImage(with: url)
    }
}
